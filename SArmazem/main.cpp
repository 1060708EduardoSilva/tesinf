/* 
 * File:   main.cpp
 * Author: rsw
 *
 * Created on 15 de Outubro de 2014, 17:04
 */

#include <cstdlib>
#include "Deposito.h"
#include "DNormal.h"
#include "Produto.h"
#include "Armazem.h"
#include <vector>
#include <typeinfo>
#include <stdio.h>
using namespace std;

// string nomeProdut;
//    int choice;
//    int ndepNorm;
//    int ndepFresc;
//    int nPalNorm;
//    int nPalFresc;
//    int capMaxPalet;

/*
 * 
 */

void recebeDepositosOD(int &o, int &d) {
    cout << "Insira o Depósito de origem: ";
    cin >> o;
    cout << "Insira o Depósito de destino: ";
    cin >> d;
}

void recebeDistancia(int &dt) {
    do {
        cout << "Insira a Distância: ";
        cin >> dt;
        if (dt <= 0)
            cout << "Distância inválida!!!";
    } while (dt <= 0);

}

int main(int argc, char** argv) {
    string nomeProdut;
    int choice;
    int ndepNorm;
    int ndepFresc;
    int nPalNorm;
    int nPalFresc;
    int capMaxPalet;
    int deposito_t;
    int quatd;

    ///NECESSARIOS PARA CONSTRUTOR ARMAZEM RANDOM
    
    int nMaxDep;
    int nMinDep;
    int nMinPal;
    int nMaxPal;
    int distMin;
    int distMax;
    int capMinPal;
    int capMaxPal;
    
    ///FIM DE NECESSARIOS PARA ARMAZEM RANDOM

    vector<Deposito*> vec;

    list<Ligacao*> ligacoes;

    Armazem a1;
    
    
    int o = 0, d = 0, dt = 0;
    do {
        cout << "\nMENU:";
        cout << endl
                << " 1  - Gerar Armazem (max/min)\n"
                << " 2  - Inserir Produto\n"
                << " 3  - Inserir vários produtos\n"
                << " 4  - Expedir produto\n"
                << " 5  - Expedir vários produtos\n"
                << " 6  - Visualizar Mapa de depósito\n"
                << " 7  - Construir grafo\n"
                << " 8  - Exportar armazém para ficheiro de texto\n"
                << " 9  - Importar armazém de ficheiro de texto\n"
                << " 10 - Mostrar Percurso mais curto entre dois depósitos\n"
                << " 11 - Mostrar todos os Percursos entre dois depósitos\n"
                << " 12 - Mostrar Percursos entre depósitos de um tipo\n"
                << " 0  - Sair\n"
                << " Digite a sua escolha e pressione [ENTER]: ";
        cin >> choice;
        if (cin.fail()){
             cin.clear();
              std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << "\nDigite um número entre 0 e 12...\n";
            choice = 20;
        }

        switch (choice) {
            case 1:
                cout << "Gerar Armazem (max/min)\n";
                cout << "Indique o numero maximo de depositos:";
                cin>> nMaxDep;
                cout << "Indique o numero minimo de depositos:";
                cin>> nMinDep;
                cout << "Indique o numero maximo de paletes:";
                cin>> nMaxPal;
                cout << "Indique o numero minimo de paletes:";
                cin>> nMinPal;
                cout << "Indique a distancia maximo entre depositos:";
                cin>> distMax;
                cout << "Indique a distancia minima entre depositos:";
                cin>> distMin;
                cout << "Indique a capacidade maxima da palete:";
                cin>> capMaxPal;
                cout << "Indique a dcapacidade minima da palete:";
                cin>> capMinPal;
                a1.criaArmazemRandom(nMinDep, nMaxDep, nMinPal, nMaxPal, capMinPal, capMaxPal, distMin, distMax);
                break;
            case 2:
                cout << "inserir produto\n";
                cout << "nome para o produto: ";
                cin>> nomeProdut;
                cout << "qual o deposito para inserir o produto:";
                cin>> deposito_t;
                a1.inserirProdutoNoDeposito(new Produto(nomeProdut), deposito_t);
                break;
            case 3:
                cout << "\nQue quantidade de produtos a inserir?:";
                cin>> quatd;
                cout << "\nEm que deposito quer inserir os produtos:";
                cin>> deposito_t;
                a1.inserirProdutosNoDeposito(quatd, deposito_t);
                break;
            case 4:
                cout << "De qual deposito quer expedir o produto?:";
                cin>> deposito_t;
                a1.expedirProdutoDoDeposito(deposito_t);
                break;
            case 5:
                cout << "\nQue quantidade de produtos quer expedir?:";
                cin>> quatd;
                cout << "\nDe qual deposito quer expedir os produtos:";
                cin>> deposito_t;
                a1.expedirProdutosDoDeposito(deposito_t, quatd);
                break;
            case 6:
                cout << "Imprimir mapa depositos...\n";
                a1.imprimirMapaDeDepositos();
                break;
            case 7:
                cout << "Construir grafo...";
                a1.limpaGrafo();
                a1.constroiGrafo();
                break;
            case 8:
                cout << "Exportar armazém para ficheiro de texto...";
                a1.escreveDepositosParaFicheiro("nome");
                a1.escreveLigacoesParaFicheiro("nome");
                a1.guardarMapaDeDepositosTextoFinal("nome");
                break;
            case 9:
                a1.limpaGrafo();
                a1.setArmz(vec);
                a1.setLigacoes(ligacoes);
                cout << "Importar armazém de ficheiro de  texto...";
                a1.leDepositosDoFicheiro("nome");
                a1.leLigacoesDoFicheiro("nome");
                //                a1.constroiGrafo();
                break;
            case 10:
                recebeDepositosOD(o, d);
                a1.percursoMaisCurto2Depositos(o, d);
                break;
            case 11:
                recebeDepositosOD(o, d);
                a1.percursosPossiveis2Depositos(o, d);
                break;
            case 12:
                recebeDepositosOD(o, d);
                a1.percursosPossiveis2DepositosMesmoTipo(o, d);
                break;
            case 0:
                cout << "Fim de Programa\n";
                break;
            default:
                cout << "Opção INVÁLIDA. \n"
                        << "Digite, novamente a sua escolha e pressione [ENTER]: ";
                break;
        }

    } while (choice != 0);
    return 0;
}
