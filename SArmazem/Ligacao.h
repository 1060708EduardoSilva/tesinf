/* 
 * File:   Ligacao.h
 * Author: rsw
 *
 * Created on 24 de Novembro de 2014, 22:55
 */

#ifndef LIGACAO_H
#define	LIGACAO_H
#include <iostream>
#include <string>
#include "Deposito.h"
using namespace std;

class Ligacao {
protected:
    Deposito* depositoOrigem;
    Deposito* depositoDestino;
    int distancia;

public:
    Ligacao();
    Ligacao(Deposito* dOrigem, Deposito* dDestino, int dist);
    ~Ligacao();
    
    Ligacao* clone();
    void escreve(ostream& out) const;
    void setDistancia(int distancia);
    int getDistancia() const;
    void setDepositoDestino(Deposito* depositoDestino);
    Deposito* getDepositoDestino() const;
    void setDepositoOrigem(Deposito* depositoOrigem);
    Deposito* getDepositoOrigem() const;
    bool operator<(const Ligacao&) const;
    bool operator>(const Ligacao&) const;
    bool operator==(const Ligacao&) const;
    Ligacao operator =(const Ligacao&);
    Ligacao operator +(const Ligacao&);
    Ligacao operator +=(const Ligacao&);
    
};

/*
 *Constructor
 * s/ parametros
 */
Ligacao::Ligacao() {
    setDepositoOrigem(NULL);
    setDepositoDestino(NULL);
    setDistancia(0);
}

/*
 *Constructor
 * c/ parametros
 */
Ligacao::Ligacao(Deposito* dOrigem, Deposito* dDestino, int dist) {
    setDepositoOrigem(dOrigem);
    setDepositoDestino(dDestino);
    setDistancia(dist);
}

/*Destr*/
Ligacao::~Ligacao(){//cout << "Ligacao Deleted\n ";
    
}

/*Clone*/
Ligacao* Ligacao::clone() {
    return new Ligacao(*this);
}


///*PRINT*/
void Ligacao::escreve(ostream& out) const {
    out << "Ligacao_" <<depositoOrigem->getChave()<<"_"<<depositoDestino->getChave()<<"_"<<distancia << endl;
//	out << "##LIGACAO";
//	out << "     Chave deposito de Origem: " << depositoOrigem->getChave();
//	out << "     Chave deposito de Destino: " << depositoDestino->getChave();
//	out << "     Distancia: " << distancia << endl;
}


/*sets e gets*/
void Ligacao::setDistancia(int distancia) {
    this->distancia = (distancia >= 0) ? distancia : 0;
}

int Ligacao::getDistancia() const {
    return distancia;
}

void Ligacao::setDepositoDestino(Deposito* depositoDestino) {
    this->depositoDestino = depositoDestino;
}

Deposito* Ligacao::getDepositoDestino() const {
    return depositoDestino;
}

void Ligacao::setDepositoOrigem(Deposito* depositoOrigem) {
    this->depositoOrigem =  depositoOrigem;
}

Deposito* Ligacao::getDepositoOrigem() const {
    return depositoOrigem;
}


/*operadores*/
bool Ligacao::operator <(const Ligacao& ligacao) const {
	return this->distancia < ligacao.getDistancia();
}

bool Ligacao::operator >(const Ligacao& ligacao) const {
	return this->distancia > ligacao.getDistancia();
}

bool Ligacao::operator ==(const Ligacao& ligacao) const {
	return this->distancia == ligacao.getDistancia();
}

//void Ligacao::escreve(ostream& out) const {
//	out     << "Distancia: " << distancia << "\n";
//}

ostream& operator <<(ostream& out , const Ligacao& l) {
	l.escreve(out);
	return out;
}

Ligacao Ligacao::operator +(const Ligacao &lig) {
    return Ligacao(this->getDepositoOrigem(), (lig).getDepositoDestino(), (this->getDistancia()+(lig).getDistancia()));
}

Ligacao Ligacao::operator+=(const Ligacao &lig) {

    setDepositoOrigem(this->getDepositoDestino());
    setDepositoDestino(lig.depositoDestino);

    setDistancia((this->getDistancia()+(lig).getDistancia()));
    return *this;
}


Ligacao Ligacao::operator =(const Ligacao &op){
	if(this != &op){
		setDepositoOrigem(op.depositoOrigem);
		setDepositoDestino(op.depositoDestino);
		setDistancia(op.distancia);
	}
	return *this;
}

#endif	/* LIGACAO_H */

