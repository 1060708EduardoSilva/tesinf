/* 
 * File:   DNormal.h
 * Author: rsw
 *
 * Created on 14 de Outubro de 2014, 15:47
 */

#ifndef DNORMAL_H
#define	DNORMAL_H
#include "Deposito.h"
#include <vector>
#include <stack>
#include <iostream>
#include<list>
#include "Produto.h"
using namespace std;


class DNormal : public Deposito {
private:
    vector<stack<Produto*> > depos;
public:
    DNormal(int, int, int, float);
    virtual ~DNormal();
    virtual void inserirPaletes();
    int encontraPaleteComMenosProdutos();
    void inserirProdutoNaPalete(int, Produto*);
    int espacoDisponivelNaPalete(int);
    virtual void inserirProduto(Produto*);
    virtual bool expedirProduto();
    virtual void inserirProdutos(list<Produto*>);
    virtual void expedirProdutos(int);
    void setDepos(vector<stack<Produto*> > depos);
    vector<stack<Produto*> > getDepos() const;
    virtual int nProdutosNoDeposito() const; // retorna o numero de produtos no deposito.
    bool operator <(Deposito& d);
    virtual void escreve(ostream&) const;
};



DNormal::DNormal(int a, int b, int c, float d) : Deposito(a, b, c, d) {
        cout << "teste - com capacidade para " << this->getNPaletes();
        inserirPaletes();
}


DNormal::~DNormal(){
    //cout << "DNormal deleted \n";
}



void DNormal::inserirPaletes(){
    int capMax;
    for (int i = 0; i < this->getNPaletes(); i++) {
        if (i % 2 == 0) {
            stack<Produto*> p1;
            this->depos.push_back(p1);
            capMax = this->getCapMaxPalete();
        } else {
            stack<Produto*> p1;
            this->depos.push_back(p1);
            capMax = ((this->getCapMaxPalete()) / 2);
        }
       cout << "inseri palete nº " << i << "com capacidade para " << capMax << " produtos \n";
   }
    cout << "TESTE SE PREENCHE COM PALTES \n Numroe de Paletes inseridas = " << this->depos.size() << "\n";
}


void DNormal::inserirProduto(Produto* prod){
    int pComMenosProdutos = encontraPaleteComMenosProdutos();
    inserirProdutoNaPalete(pComMenosProdutos, prod);
}

void DNormal::inserirProdutoNaPalete(int index, Produto* prod){
    depos.at(index).push(prod);
    cout << "Inserido na Palete " << index << " o produto " << prod->GetNome() << "\n"; 
}


void DNormal::inserirProdutos(list<Produto*> l) {
    for (Produto* p : l) {
        inserirProduto(p);
    }
}




int DNormal::encontraPaleteComMenosProdutos() {  //retorna o indice da palete com menos produtos conforme regra de inserção de produtos
    int maxProdutos = -1;
    int it = -1;

    
    for (int i = 0; i < depos.size(); i = i + 2) { //verifica se nas paletes par existe espaço para inserir o produto
        if (espacoDisponivelNaPalete(i) != 0) {
           
            maxProdutos = this->getCapMaxPalete()-(espacoDisponivelNaPalete(i));
            it = i;
            cout << " Encontrei na palete " << i << ", " << maxProdutos << " produtos de " << this->getCapMaxPalete() << " possiveis \n";
            i = depos.size();
        }
    }

    if (it == (-1)) { //se o retorno for -1, entao não existem paletes par onde seja possivel inserir.
        for (int i = 1; i < depos.size(); i + 2) { //verifica nas paletes impar se existe espaço em alguma daspaletes impar para completar
            if (espacoDisponivelNaPalete(i) != 0) {
                maxProdutos = ((this->getCapMaxPalete()) / 2)-(espacoDisponivelNaPalete(i));
                it = i;
                cout << " Encontrei na palete " << i << ", " << maxProdutos << " produtos de " << this->getCapMaxPalete() / 2 << " possiveis \n";
                i = depos.size();
            }
        }
    }

    return it;
}


int DNormal::espacoDisponivelNaPalete(int indexPalete){  // retorna o espaço disponivel na palete
    if(indexPalete%2 == 0){
        return (this->getCapMaxPalete() - (this->depos.at(indexPalete).size())); 
    }else{
        return ((this->getCapMaxPalete()/2) - (this->depos.at(indexPalete).size()));
    }
}


bool DNormal::expedirProduto() {
    bool teste = false;
    int val = depos.size() % 2;
    cout << val;


    for (int i = depos.size() - 1 - val; i > -1; i = (i - 2)) { /// se for PAR
        if ((espacoDisponivelNaPalete(i)) < (this->getCapMaxPalete())/2) {
            cout << "removi da palete " << i << " o produto " << depos.at(i).top()->GetNome() << "\n";
            depos.at(i).pop();
            teste = true;
            i = -1;
        }
    }

    if (teste == false) {
        for (int i = depos.size() - val; i > -1; i = (i - 2)) {
            if ((espacoDisponivelNaPalete(i)) < this->getCapMaxPalete()) {
                cout << "removi da palete " << i << " o produto " << depos.at(i).top()->GetNome() << "\n";
                depos.at(i).pop();
                teste = true;
                i = -1;
            }

        }
    }
    if (teste == false) {
        cout << "não foi possivel expedir o produto [PALETES NORMAIS VAZIAS] \n";
    }
    return teste;
}

void DNormal::expedirProdutos(int nProdutos) {
    if(!(nProdutosNoDeposito()>nProdutos)){
        for(int i = 0; i < nProdutos; i++){
            expedirProduto();
        }
    }else{
        cout << "Não é possivel retirar todos os Produtos deste deposito. (O deposito só contem " << nProdutosNoDeposito() << "produtos)\n";
    }
}


////SET's e GET's


void DNormal::setDepos(vector<stack<Produto*>  > depos) {
    this->depos = depos;
}


vector<stack<Produto*> > DNormal::getDepos() const {
    return depos;
}


int DNormal::nProdutosNoDeposito()const {
    int nProdutos = 0;
    for(int i = 0; i < this->depos.size(); i++){
        nProdutos += this->depos.at(i).size();
    }
    return nProdutos;
}

//bool DNormal::operator < (Deposito& d){
//    return (this->nProdutosNoDeposito() < (d.nProdutosNoDeposito()));
//}


void DNormal::escreve(ostream& out) const {
	out     << "##DEPOSITO NORMAL\t| Chave: " << this->getChave() << "\n";
//                << "Numero de Paletes: " << this->getNPaletes() << "\n"
//                << "Capacidade maxima Palete: " << this->getCapMaxPalete() << "\n"
//                << "Area total ocupada:  " << this->getAreaTotalOcupada() << "\n";
}

ostream& operator <<(ostream& out , const DNormal& d) {
	(d).escreve(out);
	return out;
}



#endif	/* DNORMAL_H */

