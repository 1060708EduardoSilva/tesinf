/* 
 * File:   Deposito.h
 * Author: rsw
 *
 * Created on 14 de Outubro de 2014, 15:46
 */

#ifndef DEPOSITO_H
#define	DEPOSITO_H
#include <list>
#include <deque>
#include <string>
#include <vector>
#include <iostream>
#include "Produto.h"
#include "graphEdge.h"
using namespace std;


class Deposito{
    
private:
    int chave;
    int nPaletes;
    int capMaxPalete;
    float areaTotalOcupada;
    
    
public:
    bool operator<(const Deposito& d);
    bool operator>(const Deposito& d);
    bool operator==(const Deposito& d);
    Deposito();
    Deposito(int, int, int,  float);
    virtual ~Deposito();
    virtual void inserirPaletes()=0;
//    virtual void imprimirProdutos()=0;
    void setNPaletes(int);
    virtual bool expedirProduto()=0;
    virtual void inserirProdutos(list<Produto*>) = 0;
    virtual void inserirProduto(Produto* )= 0;
    virtual void expedirProdutos(int)= 0;
    virtual int nProdutosNoDeposito() const = 0;
    int getNPaletes() const;
    int getCapMaxPalete() const;
    void setCapMaxPalete(int);
    void setChave(int chave);
    int getChave() const;
    virtual void escreve(ostream&) const;
    friend ostream& operator<<(ostream& out, const Deposito& d);
    float getAreaTotalOcupada() const;
};


Deposito::Deposito(int a, int b, int c, float d){
    chave = a;
    nPaletes = b;
    capMaxPalete = c;
    areaTotalOcupada = d;
}
//
//void Deposito::inserirProdutos(list<Produto> l){
//    cout << "BASE -> ";
//}
//
//void Deposito::expedirProduto(Produto p){
//    cout << "BASE -> ";
//}

Deposito::~Deposito(){//cout << "Morreu deposito \n";
    
}

bool Deposito::operator<(const Deposito& d){
    if (this->nProdutosNoDeposito() < d.nProdutosNoDeposito()) {
        return true;
    } else {
        return false;
    }
}

bool Deposito::operator>(const Deposito& d){
    if (this->nProdutosNoDeposito() > d.nProdutosNoDeposito()) {
        return true;
    } else {
        return false;
    }
}

bool Deposito::operator==(const Deposito& d){
    if (this->nProdutosNoDeposito() == d.nProdutosNoDeposito()) {
        return true;
    } else {
        return false;
    }
}






















//SET's and GET's
//void Deposito::SetAreaTotalOcupada(float areaTotalOcupada) {
//    this->areaTotalOcupada = areaTotalOcupada;
//}
//
//float Deposito::GetAreaTotalOcupada() const {
//    return areaTotalOcupada;
//}
//
void Deposito::setCapMaxPalete(int capMaxPalete) {
    this->capMaxPalete = capMaxPalete;
}
int Deposito::getCapMaxPalete() const {
    return capMaxPalete;
}


void Deposito::setNPaletes(int nPaletes) {
    this->nPaletes = nPaletes;
}

int Deposito::getNPaletes() const {
    return nPaletes;
}

//
void Deposito::setChave(int chave) {
    this->chave = chave;
}
//
int Deposito::getChave() const {
    return chave;
}

float Deposito::getAreaTotalOcupada() const{
    return areaTotalOcupada;
}


void Deposito::escreve(ostream& out) const {
	out     << "Chave: " << chave << "\n"
                << "Numero de Paletes: " << nPaletes << "\n"
                << "Capacidade maxima Palete: " << capMaxPalete << "\n"
                << "Area total ocupada:  " << areaTotalOcupada << "\n";
}

ostream& operator <<(ostream& out , const Deposito& d) {
	d.escreve(out);
	return out;
}

#endif	/* DEPOSITO_H */

