/* 
 * File:   Armazem.h
 * Author: rsw
 *
 * Created on 15 de Outubro de 2014, 22:17
 */

#ifndef ARMAZEM_H
#define	ARMAZEM_H
#include <list>
#include <string>
#include <map>
#include <vector>
#include <typeinfo>
#include <fstream>
#include "Deposito.h"
#include "DNormal.h"
#include "DFresco.h"
#include "Produto.h"
#include "graphStlPath.h"
#include "Ligacao.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <time.h> 


using namespace std;

class Armazem : public graphStlPath<Deposito*, Ligacao> {
private:
    int nDNormal;
    int nDFresco;
    int nPaletesNormal;
    int nPaletesFresco;
    int distMaxEntreDepos;
    int distMinEntreDepositos;
    int capacidadeMaxPalete;
    int nChavesDepositos = 0;
    vector<Deposito* > armz;
    map<int, map<int, int> > myMap; //Origem, destino, tamanho
    list<Ligacao*> ligacoes;

public:
    void inserirProdutosNoDeposito(int, int);
    void inserirDepositos(int); //Recebe como inteiro a opção para inserir produtos de forma automatica/manual
    void inserirProdutoNoDeposito(Produto*, int);
    Armazem();
    Armazem(int, int, int, int, int);
    Armazem(int, int, int, int, int, int, int, int);
    Armazem(vector<Deposito*>, list<Ligacao*>);
    ~Armazem();
    void constroiGrafo();
    void setArmz(vector<Deposito*> armz);
    void setLigacoes(list<Ligacao*> depositos);
    vector<Deposito*> getArmz() const;
    list<Ligacao *> getLigacoes() const;
    void expedirProdutoDoDeposito(int);
    int retornaIndexPorChave(int);
    void expedirProdutosDoDeposito(int, int);
    void pDepositosDistancias(int);
    void pDepositosDistancias2(int, int, int);
    void imprimirMapaDeDepositos();
    void guardarMapaDeDepositosEmTexto();
    int retornaNAleatorioEntre2Numeros(int val1, int val2); //RETORNA ALEATORIO ENTRE 2 NUMEROS
    int retornaNAleatorio(int val1); //RETORNA ALEATORIO por um numero maximo;
    void percursoMaisCurto2Depositos(int chaveOrig, int chaveDest);
    void percursosPossiveis2Depositos(int chaveOrig, int chaveDest);
    void leLigacoesDoFicheiro(string text);
    void leDepositosDoFicheiro(string text);
    void escreveLigacoesParaFicheiro(string nome);
    void escreveDepositosParaFicheiro(string nome);
    bool retornaPtrDepositoPrChave(int chave, Deposito* deposito);
    Deposito* retornaPtrDepositoPrChave2(vector<Deposito *> depositos, int chave);
    void percursosPossiveis2DepositosMesmoTipo(int chaveOrig, int chaveDest);
    void adLigacao(int o, int d, int dt);
    void guardarMapaDeDepositosTextoFinal(string nome);
    void criaArmazemRandom(int nMinDepositos, int nMaxDepositos, int nMinPaletes, int nMaxPaletes, int capMinPalete, int capMaxPalete, int distanciaMinEntreDepositos, int distanciaMaxEntreDepos);
    void limpaGrafo();
};

Armazem::Armazem() {
    nDNormal = 0;
    nDFresco = 0;
    nPaletesNormal = 0;
    nPaletesFresco = 0;
    capacidadeMaxPalete = 0;
}

Armazem::Armazem(int numDNormal, int numDFresco, int numPaletesNormal, int numPaletesFresco, int capMaxPalete) {
    nDNormal = numDNormal;
    nDFresco = numDFresco;
    nPaletesNormal = numPaletesNormal;
    nPaletesFresco = numPaletesFresco;
    capacidadeMaxPalete = capMaxPalete;
    inserirDepositos(0);
}

Armazem::Armazem(int nMinDepositos, int nMaxDepositos, int nMinPaletes, int nMaxPaletes, int capMinPalete, int capMaxPalete, int distanciaMinEntreDepositos, int distanciaMaxEntreDepos) {
    srand(time(NULL));
    nDNormal = retornaNAleatorioEntre2Numeros(nMinDepositos, nMaxDepositos); //VERIFICAR SE E SUPOSTO O nDNormal+nDFresco = NUMERO MAXIMO DE DEPOSITOS NO ARMAZEM
    nDFresco = retornaNAleatorioEntre2Numeros(nMinDepositos, nMaxDepositos);
    nPaletesNormal = retornaNAleatorioEntre2Numeros(nMinDepositos, nMaxDepositos);
    nPaletesFresco = nPaletesNormal;
    capacidadeMaxPalete = retornaNAleatorioEntre2Numeros(capMinPalete, capMaxPalete);
    distMaxEntreDepos = distanciaMaxEntreDepos;
    distMinEntreDepositos = distanciaMinEntreDepositos;

    cout << "#NUMERO DE DEPOSITOS NORMAIS A SER CRIADOS: " << nDNormal << "\n#NUMERO DE DEPOSITOS FRESCOS A SER CRIADOS: " << nDFresco << "\n#NUMERO DE PALETES TIPO NORMAL A SER CRIADAS: " << nPaletesNormal << "\n#NUMERO DE PALETES DO TIPO FRESCO A SER CRIADAS: " << nPaletesFresco << "\n#CAPACIDADE MAXIMA POR PALETE: " << capacidadeMaxPalete << "\n";
    inserirDepositos(1);
}

Armazem::Armazem(vector<Deposito*> depositos, list<Ligacao*> ligacoes) {
    setArmz(depositos);
    setLigacoes(ligacoes);
}

Armazem::~Armazem() {
    for (int i = 0; i < armz.size(); i++) {
        delete armz.at(i);
    }
}

void Armazem::limpaGrafo(){
    for(int i = 0; i < this->vlist.size(); i++){
        vlist.pop_front();        
    }
    this->resetKeys();
}

void Armazem::inserirDepositos(int val) {
    //srand(time(NULL));
    //    DNormal d1=new DNormal(1, nPaletesNormal, capacidadeMaxPalete, 10.01);
    //    DNormal d2=new DNormal(2, nPaletesNormal, capacidadeMaxPalete, 10.01);
    //    armz.push_back(new DNormal(1, nPaletesNormal, capacidadeMaxPalete, 10.01));
    //    armz.push_back(new DNormal(2, nPaletesNormal, capacidadeMaxPalete, 10.01));
    //    cout << "Quantidade de depositos no armazem: " << armz.size() << "\n";
    //    
    //

    if (val == 0) {
        for (int i = 0; i < nDNormal; i++) {
            nChavesDepositos++;
            pDepositosDistancias(nChavesDepositos);
            armz.push_back(new DNormal(nChavesDepositos, nPaletesNormal, capacidadeMaxPalete, 10.01));
        }
        //Criar aqui rotina para DFRESCOS
        for (int i = 0; i < nDFresco; i++) {
            nChavesDepositos++;
            pDepositosDistancias(nChavesDepositos);
            armz.push_back(new DFresco(nChavesDepositos, nPaletesFresco, capacidadeMaxPalete, 10.01));
        }
    } else {
        for (int i = 0; i < nDNormal; i++) {
            nChavesDepositos++;
            //pDepositosDistancias2(nChavesDepositos, distMinEntreDepositos, distMaxEntreDepos);
            armz.push_back(new DNormal(nChavesDepositos, nPaletesNormal, capacidadeMaxPalete, 10.01));
            pDepositosDistancias2(nChavesDepositos, distMinEntreDepositos, distMaxEntreDepos);
        }
        //Criar aqui rotina para DFRESCOS
        for (int i = 0; i < nDFresco; i++) {
            nChavesDepositos++;
            //pDepositosDistancias2(nChavesDepositos, distMinEntreDepositos, distMaxEntreDepos);
            armz.push_back(new DFresco(nChavesDepositos, nPaletesFresco, capacidadeMaxPalete, 10.01));
            pDepositosDistancias2(nChavesDepositos, distMinEntreDepositos, distMaxEntreDepos);
        }
    }

}

void Armazem::inserirProdutoNoDeposito(Produto* p1, int index) {
    armz.at(index)->inserirProduto(p1);
}

void Armazem::expedirProdutoDoDeposito(int index) {
    armz.at(index)->expedirProduto();
}

int Armazem::retornaIndexPorChave(int chave) { // metodo que indetifica o indice do deposito pela chave. Retorna -1 se não encontrar o deposito com essa chave
    int ret = -1;
    for (int i = 0; i < armz.size(); i++) {
        if (chave == armz.at(i)->getChave()) {
            ret = i;
        }
    }
    if (ret == -1) {
        cout << "Não foi possivel encontrar o Deposito com chave " << chave << "\n";
    }
    return ret;

}

void Armazem::expedirProdutosDoDeposito(int index, int produtos) {
    if (armz.at(index)->nProdutosNoDeposito() < produtos) {
        cout << "Não é possivel retirar todos os Produtos deste deposito. (O deposito só contem " << armz.at(index)->nProdutosNoDeposito() << "produtos)\n";
    } else {
        armz.at(index)->expedirProdutos(produtos);
    }
}

void Armazem::pDepositosDistancias(int index) {//pergunta a que depositos e respectivas distancias esta ligado o depoisot com a cgave passada.
    int resp;
    int chaveDestino;
    int distanciaDestino;
    map<int, map<int, int> >::iterator it;
    map<int, map<int, int> >::iterator it3;
    map<int, int>::iterator it2;
    map<int, int>::iterator it4;
    map<int, int> map2;
    map<int, int>::iterator it6;
    bool verificaExistencia = false;

    if (myMap.empty() != true) {// valida se ja existe algum deposito ligado ao que é passado na variavel index
        for (it = myMap.begin(); it != myMap.end(); it++) {
            it2 = it->second.find(index);
            if (it2 != it->second.end()) { //Verifica se encontra, se encontrar o it2 fica a apontar.
                map2.insert(std::pair<int, int>(it->first, it2->second));
            }
        }
    }

    cout << "\n Pretende carregar ligações ao deposito " << index << "?\n";
    cin >> chaveDestino;
    do {

        ////////////
        do {
            cout << "Existe algum outro Deposito, que se encontre ligado, ao deposito " << index << ". (1 - Sim, 0 - Não)\n";
            cin >> resp;
            cout << "RESPPP " << resp << "\n";
        } while (!((resp == 0) || (resp == 1)));
        ///////////
        if (resp == 1) {
            cout << "\n Indique a CHAVE do Deposito, a que se encontra ligado, o deposito " << index << "\n";
            cin >> chaveDestino;
            ////////////
            it6 = map2.find(chaveDestino);
            ///////////
            while (chaveDestino > (nDFresco + nDNormal) || (chaveDestino == index) || (it6 != map2.end())) {
                cout << "Resposta invalida. Só existem depositos com chave até " << (nDFresco + nDNormal) << "\n";

                ////////////////
                if (it6 != map2.end()) {
                    cout << "A chave referida já foi inserida, e esta a uma distância de " << it6->second << "\n";
                }

                do {
                    cout << "Existe algum outro Deposito, que se encontre ligado, ao deposito E" << index << ". (1 - Sim, 0 - Não)\n";
                    cin >> resp;
                    cout << "RESPPP " << resp << "\n";
                    if (resp == 0) {
                        chaveDestino = -1;
                    }
                } while (!((resp == 0) || (resp == 1)));

                /////////////////

                if (resp == 1) {///
                    cout << "Indique a CHAVE do Deposito, a que se encontra ligado, o deposito  \n";
                    cin >> chaveDestino;
                    ////////////////
                    it6 = map2.find(chaveDestino);
                    //////////////////
                }////
            }

            if (resp == 1) {
                cout << "\n CHAVE DE DESTINO: " << chaveDestino << "\n";
                cout << "Indique a distancia a que os depositos se encontram \n";
                cin >> distanciaDestino;


                cout << "\n DISTANCIA DESTINO: " << distanciaDestino << "\n";

                //map2.insert(std::pair<int, int>(chaveDestino, distanciaDestino));

                it3 = myMap.find(chaveDestino); // valida se ja existe alguma chave destino que tenha a ligação a este deposito.
                if (it3 != myMap.end()) {
                    it4 = it3->second.find(index);
                    if (it4 == it3->second.end()) { //Se nao encontrou o deposito ja registado, regista o mesmo
                        it3->second.insert(std::pair<int, int>(index, distanciaDestino)); //Se nao encontrou o deposito ja registado, regista o mesmo
                        map2.insert(std::pair<int, int>(chaveDestino, distanciaDestino));
                    } else { //Se encontrou o deposito ja registado, altera a distancia destino do mesmo para a actual, caso não seja a mesma. 
                        it4->second = distanciaDestino;
                        //cout << "DISTANCIA DESTINO" << distanciaDestino << " vs DISTANCIAREAL " << it4->second << "\n";
                        it2 = map2.find(chaveDestino);
                        it2->second = distanciaDestino;

                    }
                } else {
                    map2.insert(std::pair<int, int>(chaveDestino, distanciaDestino));
                }

                cout << "O Deposito " << index << "está ligado ao deposito " << chaveDestino << " com distancia de " << distanciaDestino << "\n";

                //        do {
                //            cout << "Existe algum outro Deposito, que se encontre ligado, ao deposito " << index << ". (1 - Sim, 0 - Não)\n";
                //            cin >> resp;
                //
                //            cout << "RESPPP " << resp << "\n";
                //        } while (!((resp == 0) || (resp == 1)));
            }
        }



    } while (resp != 0);
    this->myMap.insert(std::pair<int, map<int, int> >(index, map2));
}

void Armazem::imprimirMapaDeDepositos() {
            for (Deposito* d : this->armz) {
                cout << d->getChave() << "\t";
                if (typeid (*d) == typeid (DNormal)) {
                    cout << "DN\t";
                } else {
                    cout << "DF\t";
                }
                for (Ligacao* l : this-> ligacoes) {
                    if ((l->getDepositoOrigem()->getChave()) == (d->getChave())) {
                        cout << "(" << l->getDepositoDestino()->getChave() << "," << l->getDistancia() << ")";
                    }
                }
                cout << "\n";
            }
}

void Armazem::setArmz(vector<Deposito*> armz) {
    this->armz = armz;
}

void Armazem::setLigacoes(list<Ligacao*> ligacoes) {
    this->ligacoes = ligacoes;
}

vector<Deposito*> Armazem::getArmz() const {
    return armz;
}

list<Ligacao *> Armazem::getLigacoes() const {
    return ligacoes;
}

void Armazem::guardarMapaDeDepositosEmTexto() {
    map<int, map<int, int> >::iterator it;
    map<int, int>::iterator it2;
    string tipo;
    int destino;
    int distancia;

    string output;
    int x = 0;


    int origem;
    stringstream concatena;
    concatena << "ORIGEM \t\t\t\t;;TIPO\t\t\t\t;;(DESTINO,DISTANCIA)\n";

    std::ofstream out("output.txt");

    for (it = myMap.begin(); it != myMap.end(); it++) {
        origem = it->first;
        int index = retornaIndexPorChave(origem);
        //concatena << origem << "\t\t\t\t;;"<< tipo << "\t\t\t\t;;";

        if (typeid (*armz.at(index)) == typeid (DNormal)) {
            tipo = "DNormal";
        } else {
            tipo = "DFresco";
        }

        concatena << origem << "\t\t\t\t;;" << tipo << "\t\t\t;;";

        for (it2 = (it->second).begin(); it2 != (it->second).end(); it2++) {
            destino = it2->first;
            distancia = it2->second;
            concatena << "(" << destino << "," << distancia << ");";
        }
        concatena << "\n-----------------------------------------------------------------------------------------------\n";
    }
    output = concatena.str();




    out << output;
    out.close();
}

void Armazem::pDepositosDistancias2(int index, int distMinEntreDpositos, int distMaxEntreDepositos) {//pergunta a que depositos e respectivas distancias esta ligado o depoisot com a cgave passada.
    int resp = retornaNAleatorio((nDFresco + nDNormal) - 1); //gera numero aleatorio de ligações a existir(numero maximo de depositos -1) 

    int chaveDestino;
    int distanciaDestino;
    //srand(time(NULL));
    map<int, map<int, int> >::iterator it;
    map<int, map<int, int> >::iterator it3;
    map<int, map<int, int> >::iterator it5;
    map<int, int>::iterator it2;
    map<int, int>::iterator it4;
    map<int, int>::iterator it6;
    map<int, int> map2;
    bool verificaExistencia = false;



    if (myMap.empty() != true) {// valida se ja existe algum deposito ligado ao que é passado na variavel index
        for (it = myMap.begin(); it != myMap.end(); it++) {
            it2 = it->second.find(index);
            if (it2 != it->second.end()) { //Verifica se encontra, se encontrar o it2 fica a apontar.
                map2.insert(std::pair<int, int>(it->first, it2->second));
            }
        }
    }
    int lastChaveDestino = 0;
    string tipo;
    if (typeid (*armz.at(retornaIndexPorChave(index))) == typeid (DNormal)) {
        tipo = "DNormal";
    } else {
        tipo = "DFresco";
    }
    cout << "---------------------------------------------------------------------------------------\n";
    cout << "---------------------------------------------------------------------------------------\n";
    cout << "#CHAVE ORIGEM: " << index << "\n";
    cout << "#TIPO DE DEPOSITO: " << tipo << "\n";
    cout << "#NUMERO DE LIGAÇÕES A OUTROS DEPOSITOS: " << resp << "\n";
    cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    //cout << "---------------------------------------------------------------------------------------\n";

    do {


        chaveDestino = retornaNAleatorio(nDFresco + nDNormal);
        //VERIFICAR SE DENTRO DESTA LISTA JA EXISTE A CHAVE

        while ((chaveDestino > (nDFresco + nDNormal)) || (chaveDestino == index) || (chaveDestino == lastChaveDestino) || (chaveDestino == 0)) {
            //            cout << "Resposta invalida. Só existem depositos com chave até " << (nDFresco + nDNormal) << "\n";
            // cout << "Indique a CHAVE do Deposito, a que se encontra ligado, o deposito  \n";
            chaveDestino = retornaNAleatorio(nDFresco + nDNormal);
        }



        cout << "#CHAVE DESTINO: " << chaveDestino << "\n";
        distanciaDestino = retornaNAleatorioEntre2Numeros(distMinEntreDpositos, distMaxEntreDepositos);
        cout << "#DISTANCIA: " << distanciaDestino << "\n";

        //map2.insert(std::pair<int, int>(chaveDestino, distanciaDestino));

        it3 = myMap.find(chaveDestino); // valida se ja existe alguma chave destino que tenha a ligação a este deposito.
        if (it3 != myMap.end()) {
            it4 = it3->second.find(index);
            if (it4 == it3->second.end()) { //Se nao encontrou o deposito ja registado, regista o mesmo
                it3->second.insert(std::pair<int, int>(index, distanciaDestino)); //Se nao encontrou o deposito ja registado, regista o mesmo
                map2.insert(std::pair<int, int>(chaveDestino, distanciaDestino));
            } else { //Se encontrou o deposito ja registado, altera a distancia destino do mesmo para a actual, caso não seja a mesma. 
                it4->second = distanciaDestino;
                //cout << "DISTANCIA DESTINO" << distanciaDestino << " vs DISTANCIAREAL " << it4->second << "\n";
                it2 = map2.find(chaveDestino);
                it2->second = distanciaDestino;

            }
        } else {
            map2.insert(std::pair<int, int>(chaveDestino, distanciaDestino));
        }
        lastChaveDestino = chaveDestino;

        cout << "\n O Deposito " << index << "está ligado ao deposito " << chaveDestino << " com distancia de " << distanciaDestino << "\n";
        cout << "---------------------------------------------------------------------------------------\n";

        resp--;
    } while (resp != 0);
    cout << "---------------------------------------------------------------------------------------\n";
    this->myMap.insert(std::pair<int, map<int, int> >(index, map2));
}

int Armazem::retornaNAleatorioEntre2Numeros(int val1, int val2) {//Retorna aleatorio entre um maximo e um minimo
    int dif;
    int random;
    //srand(time(NULL));

    if (val1 > val2) {
        dif = val1 - val2;
        random = rand() % dif + val2;
    } else {
        dif = val2 - val1;
        random = rand() % dif + val1;
    }
    return random;
}

int Armazem::retornaNAleatorio(int val1) {//Retorna aleatoria entre um maximo e um minimo
    int dif;
    int random;
    //srand(time(NULL));
    random = rand() % val1 + 1;

    return random;
}

void Armazem::inserirProdutosNoDeposito(int nProdutos, int val) {
    string nome;
    list<Produto*> lista;
    for (int i = 0; i < nProdutos; i++) {
        cout << "Indique o nome dos Produtos \n";
        cin >> nome;
        lista.push_back(new Produto(nome));
    }
    for (Produto* p : lista) {
        this->inserirProdutoNoDeposito(p, val);
    }
}

void Armazem::percursoMaisCurto2Depositos(int orig, int dest) {
    Deposito* tmpOrig;
    Deposito* tmpDest;
    bool tOrig = getVertexContentByKey(tmpOrig, orig);
    bool tDest = getVertexContentByKey(tmpDest, dest);


    if (tOrig && tDest) {
        cout << "### Percurso entre dois Depositos";

        if (tmpOrig && tmpDest) {
            vector <Ligacao> dist;
            vector <int> path;
            int key;
            this->getVertexKeyByContent(key, tmpDest);
            this->dijkstrasAlgorithm(tmpOrig, path, dist);


            queue <Deposito*> q = this->getDijkstrasPath(key, path);
            ////
            int cont = 0;
            while (cont < path.size()) {
               //cout << "cont: " << path[cont] << endl;
                cont++;
            }
            cout << "chave: " << key << endl;
            //// 
            if (path[key] != -1) {
                cout << "\n    - Distancia: " << dist[key].getDistancia() << endl;
                cout << "\n   Percurso: \n\n";
            }
            vector <Deposito*> vec;
            while (!q.empty()) {
                vec.push_back(q.front());
                q.pop();
            }
            int size = vec.size();
            for (int i = size - 1; i >= 0; i--) {
                this->getVertexKeyByContent(key, vec[i - 1]);
                vec[i]->escreve(cout);
            }


            cout << endl;
        }
    } else {
        cout << "\n Tens a certeza que esses depositos existem?\n Acho que não !!!\n";
    }
}

void Armazem::percursosPossiveis2Depositos(int orig, int dest) {
    Deposito* tmpOrig;
    Deposito* tmpDest;
    bool tOrig = getVertexContentByKey(tmpOrig, orig);
    bool tDest = getVertexContentByKey(tmpDest, dest);

    if (tOrig && tDest) {
        cout << "Todos os percursos possiveis entre o depósito " << tmpOrig->getChave()
                << " e o depósito " << tmpDest->getChave();
        if (tmpOrig && tmpDest) {
            queue < stack<Deposito* > > q = this->distinctPaths(tmpOrig, tmpDest);
            queue < stack<Ligacao> > v = this->getTmpQPath();
            cout << "\n   Percursos encontrados: " << q.size() << "\n\n";
            int count = 1;
            while (!q.empty()) {
                stack<Deposito*> *tmpStack = &q.front();
                stack<Ligacao> *tmpVias = &v.front();
                cout << "   Percurso " << count << ": \n" << endl;
                vector <Deposito*> vec;
                vector <Ligacao> pvec;
                while (!tmpStack->empty()) {
                    vec.push_back(tmpStack->top());
                    if (tmpStack->size() > 1) {
                        pvec.push_back(tmpVias->top());
                        tmpVias->pop();
                    }
                    tmpStack->pop();
                }
                int size = vec.size();
                for (int i = size - 1; i >= 0; i--) {
                    vec[i]->escreve(cout);
                }
                int distancia = 0;
                for (int i = 0; i < pvec.size(); i++) {
                    distancia += pvec[i].getDistancia();
                }
                cout << "##Distancia total: " << distancia;
                cout << endl << endl;
                q.pop();
                v.pop();
                count++;
            }
        }
    }else{
        cout << "\n Tens a certeza que esses depositos existem?\n Acho que não !!!\n";
    }
    }

    void Armazem::constroiGrafo() {
        vector<Deposito *>::iterator itDep;
        for (itDep = armz.begin(); itDep != armz.end(); itDep++) {
            this->addGraphVertex(*itDep);
        }

        list<Ligacao *>::iterator itLiga;
        for (itLiga = ligacoes.begin(); itLiga != ligacoes.end(); itLiga++) {

            this->addGraphEdge(*((*itLiga)), (*itLiga)->getDepositoOrigem(), (*itLiga)->getDepositoDestino());
        }
    }
    //

    void Armazem::leLigacoesDoFicheiro(string text) {
        this->ligacoes.empty();
        string line;
        char delim = '\t';
        int origem;
        int destino;
        int distancia;

        ifstream in("ligacoes_" + text + ".txt");
        if (in.is_open()) {
            //getline(in, line);
            while (getline(in, line)) {
                vector<string> res;
                istringstream iss(line);
                for (string token; getline(iss, token, delim);) {
                    res.push_back(move(token));
                }
//                cout << "\n";
//                origem = (atoi(res[0].c_str()));
//                cout << origem;
//                destino = (atoi(res[1].c_str()));
//                cout << destino;
//                distancia = (atoi(res[2].c_str()));
//                cout << distancia;
                this->ligacoes.push_back((new Ligacao(retornaPtrDepositoPrChave2(this->armz, origem), retornaPtrDepositoPrChave2(this->armz, destino), distancia)));
            }

            //} else cout << "Não foi possivel abrir o ficheiro";
            in.close();
        } else cout << "Não foi possivel abrir o ficheiro";
    }

    void Armazem::escreveLigacoesParaFicheiro(string nome) {
        list<Ligacao*>::iterator it;
        ofstream out("ligacoes_" + nome + ".txt");
        if (out.is_open()) {
            for (it = ligacoes.begin(); it != ligacoes.end(); it++) {
                out << (*it)->getDepositoOrigem()->getChave();
                out << "\t";
                out << (*it)->getDepositoDestino()->getChave();
                out << "\t";
                out << (*it)->getDistancia();
                out << "\n";
            }
            out.close();
        } else cout << "Não foi possivel abrir o ficheiro";
    }

    void Armazem::escreveDepositosParaFicheiro(string nome) {
        vector<Deposito*>::iterator it;
        ofstream out("depositos_" + nome + ".txt");
        if (out.is_open()) {
            //        out << "CHAVE";
            //        out << ";";
            //        out << "TIPO DE DEPOSITO";
            //        out << ";";
            //        out << "NUMERO DE PALETES";
            //        out << ";";
            //        out << "CAPACIDADE MAX DAS PALETES";
            //        out << ";";
            //        out << "AREA TOTAL OCUPADA";
            //        out << "\n";
            for (it = armz.begin(); it != armz.end(); it++) {
                string tipoDeposito;
                if (typeid (*(*it)) == typeid (DNormal)) {
                    tipoDeposito = "DN";
                } else {
                    tipoDeposito = "DF";
                }
                out << (*it)->getChave();
                out << "\t";
                out << tipoDeposito;
                out << "\t";
                out << (*it)->getNPaletes();
                out << "\t";
                out << (*it)->getCapMaxPalete();
                out << "\t";
                out << (*it)->getAreaTotalOcupada();
                out << "\n";
            }
            out.close();
        } else cout << "Não foi possivel abrir o ficheiro";
    }

    void Armazem::leDepositosDoFicheiro(string text) {
        this->nChavesDepositos=0;
        this->armz.empty();
        string line;
        char delim = '\t';
        int chave;
        string tipoDeposito;
        int numeroDePaletes;
        int capacidadeMaximaPalete;
        float areaTotalOcupada;
        ifstream in("depositos_" + text + ".txt");
        if (in.is_open()) {
            //getline(in, line);
            //if (line == "CHAVE;TIPO DE DEPOSITO;NUMERO DE PALETES;CAPACIDADE MAX DAS PALETES;AREA TOTAL OCUPADA") {
            while (getline(in, line)) {
                int pos;
                int lastPos = 0;

                vector<string> res;
                istringstream iss(line);
                for (string token; getline(iss, token, delim);) {
                    res.push_back(move(token));
                }
                cout << "\n";

                chave = (atoi(res[0].c_str()));
                tipoDeposito = (res[1]);
                numeroDePaletes = (atoi(res[2].c_str()));
                capacidadeMaximaPalete = (atoi(res[3].c_str()));
                areaTotalOcupada = (atof(res[3].c_str()));
                if (tipoDeposito == "DN") {
                    armz.push_back(new DNormal(chave, numeroDePaletes, capacidadeMaximaPalete, areaTotalOcupada));
                } else {
                    armz.push_back(new DFresco(chave, numeroDePaletes, capacidadeMaximaPalete, areaTotalOcupada));
                }
                //            }
                //        } else {
                //            cout << "O ficheiro não se encontra no formato ORIGEM;DESTINO;DISTANCIA";
            }
            in.close();
            //} else cout << "Não foi possivel abrir o ficheiro";
        }


    }
    //
    //bool Armazem::retornaPtrDepositoPrChave(int chave, Deposito *dp){
    //    for(int i = 0; i < armz.size(); i++){
    //        if(this->armz[i]->getChave() == chave){
    //            *dp = *(this->armz[i]);
    //            cout << "zzzzzzz";
    //            return true;
    //        }
    //    }
    //    return false;
    //}

    Deposito * Armazem::retornaPtrDepositoPrChave2(vector<Deposito*> depositos, int chave) {
        vector<Deposito *>::iterator it;
        for (it = depositos.begin(); it != depositos.end(); it++) {
            if ((*it)->getChave() == chave) {
                return (*it);
            }
        }
        return NULL;
    }

    void Armazem::percursosPossiveis2DepositosMesmoTipo(int orig, int dest) {
        Deposito* tmpOrig;
        Deposito* tmpDest;
        bool tOrig = getVertexContentByKey(tmpOrig, orig);
        bool tDest = getVertexContentByKey(tmpDest, dest);
        if(tOrig && tDest){
        if ((tmpOrig && tmpDest)) {
            string tipoDeposito;
            if (typeid (*tmpOrig) == typeid (*tmpDest)) {
                if (typeid (*tmpOrig) == typeid (DNormal)) {
                    tipoDeposito = "deposito normal";
                } else {
                    tipoDeposito = "deposito fresco";
                }
                //            cout << "Todos os percursos possiveis entre o depósito " << tmpOrig->getChave()
                //                    << " e o depósito " << tmpDest->getChave() << tipoDeposito;
                //if ((tmpOrig && tmpDest)) {
                queue < stack<Deposito* > > q = this->distinctPaths(tmpOrig, tmpDest);
                queue < stack<Ligacao> > v = this->getTmpQPath();
                int count = 1;
                int contaCaminhos = 0;
                while (!q.empty()) {
                    stack<Deposito*> *tmpStack = &q.front();
                    stack<Ligacao> *tmpVias = &v.front();
                    vector <Deposito*> vec;
                    vector <Ligacao> pvec;
                    while (!tmpStack->empty()) {
                        vec.push_back(tmpStack->top());
                        if (tmpStack->size() > 1) {
                            pvec.push_back(tmpVias->top());
                            tmpVias->pop();
                        }
                        tmpStack->pop();
                    }
                    int size = vec.size();
                    bool eDoMesmoTipo = true;
                    for (int i = size - 1; i >= 0; i--) {
                        if (typeid (*vec[i]) != typeid (*vec[0])) {
                            eDoMesmoTipo = false;
                            //cout << "Não é do mesmo tipo \n";
                            i = 0;
                        }
                    }
                    if (eDoMesmoTipo == true) {
                        contaCaminhos++;
                        for (int i = size - 1; i >= 0; i--) {
                            vec[i]->escreve(cout);
                        }
                        int distancia = 0;
                        for (int i = 0; i < pvec.size(); i++) {
                            distancia += pvec[i].getDistancia();
                        }
                        cout << "##Distancia total: " << distancia;
                        cout << endl << endl;
                    }
                    q.pop();
                    v.pop();
                    count++;
                }
                cout << "\n   Percursos encontrados entre depositos do tipo \" " << tipoDeposito << "\": " << contaCaminhos << "\n";
            } else {
                cout << "Os depositos de origem/destino não são do mesmo tipo.";
            }

        } else {
            cout << "O(s) deposito(s) não existe(m)";
        }
        }else{
        cout << "\n Tens a certeza que esses depositos existem?\n Acho que não !!!\n";
    }
    }

    void Armazem::adLigacao(int o, int d, int dt) {
        Deposito* tmpOrig;
        Deposito* tmpDest;
        bool verificaSeJaExiste = false;
        bool teste1 = getVertexContentByKey(tmpOrig, o);
        bool teste2 = getVertexContentByKey(tmpDest, d);
        for(Ligacao* l : this->ligacoes){
            if((l->getDepositoOrigem() == tmpOrig) && (l->getDepositoDestino() == tmpDest) && (l->getDistancia() == dt)){
                verificaSeJaExiste = true;
            }
        }
        
        if(verificaSeJaExiste == false){
        this->ligacoes.push_back(new Ligacao(tmpOrig, tmpDest, dt));
        }else{
            cout << "A ligacao já existe\n";
        }
        //this->nChavesDepositos++;
    }

    void Armazem::guardarMapaDeDepositosTextoFinal(string nome) {
        ofstream out("mapadepositos_" + nome + ".txt");
        if (out.is_open()) {
            for (Deposito* d : this->armz) {
                out << d->getChave() << "\t";
                if (typeid (*d) == typeid (DNormal)) {
                    out << "DN\t";
                } else {
                    out << "DF\t";
                }
                for (Ligacao* l : this-> ligacoes) {
                    if ((l->getDepositoOrigem()->getChave()) == (d->getChave())) {
                        out << "(" << l->getDepositoDestino()->getChave() << "," << l->getDistancia() << ")";
                    }
                }
                out << "\n";
            }
            out.close();
        }
    }
    
    void Armazem::criaArmazemRandom(int nMinDepositos, int nMaxDepositos, int nMinPaletes, int nMaxPaletes, int capMinPalete, int capMaxPalete, int distanciaMinEntreDepositos, int distanciaMaxEntreDepos) {
    this->nChavesDepositos = 0;   
    int nDN = retornaNAleatorioEntre2Numeros(nMinDepositos, nMaxDepositos); //VERIFICAR SE E SUPOSTO O nDNormal+nDFresco = NUMERO MAXIMO DE DEPOSITOS NO ARMAZEM
    int nDF = retornaNAleatorioEntre2Numeros(nMinDepositos, nMaxDepositos);
    int nPN = retornaNAleatorioEntre2Numeros(nMinDepositos, nMaxDepositos);
    int nPF = nPN;
    int capPalete = retornaNAleatorioEntre2Numeros(capMinPalete, capMaxPalete);
    cout << "numero DN: " << nDN;
    cout << "numero DF: " << nDF;
    cout << "numero PN: " << nPN;
    cout << "numero PF: " << nPF;
    cout << "cap. Palete: "  << capPalete;
    float area = 0.01;
    vector<Deposito* > dep;
    list<Ligacao* > lig;
    
    for(int i = 0; i < nDN; i++){
        dep.push_back(new DNormal(nChavesDepositos, nPN, capPalete, area));
        nChavesDepositos++;   
    }

    for(int i = 0; i < nDF; i++){
        dep.push_back(new DFresco(nChavesDepositos, nPN, capPalete, area));
        nChavesDepositos++;  
    }
    
    for(int i = 0; i < dep.size(); i++){
        int existeLigacao = retornaNAleatorioEntre2Numeros(0,((nChavesDepositos-1)/2));
        if(existeLigacao > 0){
            for(int j=0; j< existeLigacao; j++){
            int ligadoAdeposito = retornaNAleatorioEntre2Numeros(0, nChavesDepositos-1);
            while(ligadoAdeposito == dep[i]->getChave()){
                ligadoAdeposito = retornaNAleatorioEntre2Numeros(0, nChavesDepositos-1);
            }
            lig.push_back(new Ligacao(dep[i], dep[ligadoAdeposito], retornaNAleatorioEntre2Numeros(distanciaMinEntreDepositos, distanciaMaxEntreDepos)));
            }
            }
    }
    this->setArmz(dep);
    this->setLigacoes(lig); 
}
    
//    void Armazem::criaDeposito(){
////        string tipoDepo;
////        cout << "Indique o tipo de deposito";
////    }
////    






#endif	/* ARMAZEM_H */

