/* 
 * File:   Produto.h
 * Author: rsw
 *
 * Created on 19 de Outubro de 2014, 18:52
 */

#ifndef PRODUTO_H
#define	PRODUTO_H
#include <string>

using namespace std;



class Produto{
private:
    string nome;
public:
    Produto(string);
    void SetNome(string nome);
    string GetNome() const;
    ~Produto();
};


Produto::Produto(string param){
    nome = param;
}

Produto::~Produto(){
    cout << "Product deleted \n"; 
}

void Produto::SetNome(string nome) {
    this->nome = nome;
}

string Produto::GetNome() const {
    return nome;
}

#endif	/* PRODUTO_H */

