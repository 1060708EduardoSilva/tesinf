/* 
 * File:   DFresco.h
 * Author: rsw
 *
 * Created on 15 de Outubro de 2014, 22:28
 */

#ifndef DFRESCO_H
#define	DFRESCO_H
#include "Deposito.h"
#include <vector>
#include <queue>
#include <iostream>
#include "Produto.h"
using namespace std;

class DFresco : public Deposito {
private:
    vector<queue<Produto*> > depos;
    int insert_prt = -1;
    int delete_prt = -1;
public:
    DFresco(int, int, int, float);
    virtual ~DFresco();
    virtual void inserirPaletes();
    int encontraPaleteComMenosProdutos();
    void inserirProdutoNaPalete(int, Produto*);
    int espacoDisponivelNaPalete(int);
    virtual void inserirProduto(Produto*);
    virtual bool expedirProduto();
    virtual void inserirProdutos(list<Produto*>);
    virtual void expedirProdutos(int);
    void setDepos(vector<queue<Produto*> > depos);
    vector<queue<Produto*> > getDepos() const;
    virtual int nProdutosNoDeposito() const;
    int getDelete_prt() const;
    int getInsert_prt() const; // retorna o numero de produtos no deposito.
    virtual void escreve(ostream&) const;
};

DFresco::DFresco(int a, int b, int c, float d) : Deposito(a, b, c, d) {
    cout << "teste - com capacidade para " << this->getNPaletes();
    inserirPaletes();
}

DFresco::~DFresco() {
    //cout << "DNormal deleted \n";
}

void DFresco::inserirPaletes() {
    int capMax;
    for (int i = 0; i < this->getNPaletes(); i++) {
        queue<Produto*> p1;
        this->depos.push_back(p1);
        capMax = this->getCapMaxPalete();

        cout << "inseri palete nº " << i << "com capacidade para " << capMax << " produtos \n";
    }
    cout << "TESTE SE PREENCHE COM PALTES \n Numroe de Paletes inseridas = " << this->depos.size() << "\n";
}

void DFresco::inserirProduto(Produto* prod) {

    if (insert_prt == (-1) && delete_prt == (-1)) {
//        delete_prt = 0;
    }

    if (insert_prt < depos.size() - 1) {
        insert_prt++;
    } else {
        insert_prt = 0;
    }
    inserirProdutoNaPalete(insert_prt, prod);
}

void DFresco::inserirProdutoNaPalete(int index, Produto* prod) {
    depos.at(insert_prt).push(prod);
    cout << "Inserido na Palete " << index << " o produto " << prod->GetNome() << "\n";
}

void DFresco::inserirProdutos(list<Produto*> l) {
    for (Produto* p : l) {
        inserirProduto(p);
    }
}


int DFresco::espacoDisponivelNaPalete(int indexPalete) { // retorna o espaço disponivel na palete
    return (this->getCapMaxPalete() - (this->depos.at(indexPalete).size()));
}

bool DFresco::expedirProduto() {
    
    bool teste = false;
    if (insert_prt == -1) {
        cout << "não foi possivel expedir o produto [DEPÓSITO VAZIO] \n";
    } else {

        if (delete_prt < depos.size() - 1) {
            delete_prt++;
        } else {
            delete_prt = 0;
        }
        cout << "removi da palete " << delete_prt << " o produto " << depos.at(delete_prt).front()->GetNome() << "\n";
        depos.at(delete_prt).pop();
        teste = true;
        if (nProdutosNoDeposito() == 0) {
            delete_prt = -1;
            insert_prt = -1;
        }
    }
//    cout << "Insert ponteirinhu!!! " << insert_prt;
//    cout << "Delete ponteirinhu!!! " << delete_prt;
    return teste;
}

void DFresco::expedirProdutos(int nProdutos) {
    if (!(nProdutosNoDeposito() < nProdutos)) {
        for (int i = 0; i < nProdutos; i++) {
            expedirProduto();
        }
    } else {
        cout << "Não é possivel retirar todos os Produtos deste deposito. (O deposito só contem " << nProdutosNoDeposito() << " produtos)\n";
    }
}


////SET's e GET's

void DFresco::setDepos(vector<queue<Produto*> > depos) {
    this->depos = depos;
}

vector<queue<Produto*> > DFresco::getDepos() const {
    return depos;
}

int DFresco::nProdutosNoDeposito() const{
    int nProdutos = 0;
    for (int i = 0; i < this->depos.size(); i++) {
        nProdutos += this->depos.at(i).size();
    }
    return nProdutos;
}

int DFresco::getDelete_prt() const {
    return delete_prt;
}

int DFresco::getInsert_prt() const {
    return insert_prt;
}

void DFresco::escreve(ostream& out) const {
out     << "|##DEPOSITO FRESCO \t| Chave: " << this->getChave() << "\n";
}

ostream& operator <<  (ostream& out , const DFresco& d) {
	d.escreve(out);
	return out;
}




#endif	/* DFRESCO_H */

